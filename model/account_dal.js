var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query ='SELECT * FROM account';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getAllAdd = function(callback) {
    var query ='CALL account_getall()';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query ='CALL account_getbyid(?)';
    var queryData = [account_id];
    console.log(query);
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


// delete functions:
//declare the function so it can be used locally
var accountSkillDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_skill WHERE account_id = ?';
    var queryData = [account_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
module.exports.accountSkillDeleteAll = accountSkillDeleteAll;

var accountSchoolDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_school WHERE account_id = ?';
    var queryData = [account_id];
    connection.query(query, queryData, function(err, result) {
        console.log('inside of school delete all');
        callback(err, result);
    });
};
module.exports.accountSchoolDeleteAll = accountSchoolDeleteAll;

var accountCompanyDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_company WHERE account_id = ?';
    var queryData = [account_id];
    console.log('inside of delete1');
    connection.query(query, queryData, function(err, result) {
        console.log('inside of delete2 ERRORS BEFORE PRINTING THIS');
        callback(err, result);
    });
};
module.exports.accountCompanyDeleteAll = accountCompanyDeleteAll;


//declare the function so it can be used locally
var accountSkillInsert = function(account_id, skillIdArray, callback){
    var query = 'INSERT INTO account_skill (skill_id, account_id) VALUES ?';
    var accountSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            accountSkillData.push([skillIdArray[i], account_id]);
        }
    }
    else {
        accountSkillData.push([skillIdArray, account_id]);
    }
    connection.query(query, [accountSkillData], function(err, result){
        callback(err, result);
    });
};
module.exports.accountSkillInsert = accountSkillInsert;

var accountSchoolInsert = function(account_id, schoolIdArray, callback){
    var query = 'INSERT INTO account_school (school_id, account_id) VALUES ?';
    var accountSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            accountSchoolData.push([schoolIdArray[i], account_id]);
        }
    }
    else {
        accountSchoolData.push([schoolIdArray, account_id]);
    }
    connection.query(query, [accountSchoolData], function(err, result){
        callback(err, result);
    });
};
module.exports.accountSchoolInsert = accountSchoolInsert;

var accountCompanyInsert = function(account_id, companyIdArray, callback){
    var query = 'INSERT INTO account_company (company_id, account_id) VALUES ?';
    console.log('passed1!');
    var accountCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            accountCompanyData.push([companyIdArray[i], account_id]);
            //console.log(`Passed ${i}`);
        }
    }
    else {
        accountCompanyData.push([companyIdArray, account_id]);
    }
    connection.query(query, [accountCompanyData], function(err, result){
        console.log('inside of loop');
        callback(err, result);
    });
};
module.exports.accountCompanyInsert = accountCompanyInsert;

/*
exports.update = function(params, callback) {
    console.log(params);
    var query = 'UPDATE account SET first_name = ?, last_name = ?, email = ? WHERE account_id = ?';
    var queryData = [params.first_name, params.last_name, params.email, params.account_id];
    connection.query(query, queryData, function(err, result) {
        accountSkillDeleteAll(params.account_id, function(err, result){
            if(params.skill_id != null) {
                accountSkillInsert(params.account_id, params.skill_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
            accountSchoolDeleteAll(params.account_id, function(err, result){
                if(params.school_id != null) {
                    accountSchoolInsert(params.account_id, params.school_id, function(err, result){
                        callback(err, result);
                    });}
                else {
                    console.log('inside of delete all else');
                    callback(err, result);
                }
                accountCompanyDeleteAll(params.account_id, function(err, result){
                    console.log('inside of delete acc comp (before insert.');
                    if(params.company_id != null) {
                        accountCompanyInsert(params.account_id, params.company_id, function(err, result){
                            callback(err, result);
                        });}
                    else {
                        callback(err, result);
                    }
                });
            });
        });
    });
};
*/

exports.update = function(params, callback) {
    console.log(params);
    var query = 'UPDATE account SET first_name = ?, last_name = ?, email = ? WHERE account_id = ?';
    var queryData = [params.first_name, params.last_name, params.email, params.account_id];
    connection.query(query, queryData, function(err, result) {
        accountSkillDeleteAll(params.account_id, function(err, result){
            accountSkillInsert(params.account_id, params.skill_id, function(err, result){
                accountSchoolDeleteAll(params.account_id, function(err, result){
                    accountSchoolInsert(params.account_id, params.school_id, function(err, result){
                        accountCompanyDeleteAll(params.account_id, function(err, result){
                            accountCompanyInsert(params.account_id, params.company_id, function(err, result){
                                    callback(err, result);
                            });
                        });
                    });
                });
            });
        });
    });
};


exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.insert = function(params, callback) {
    var query = 'INSERT INTO account (first_name, last_name, email) VALUES (?, ?, ?)';
    var queryData = [params.first_name, params.last_name, params.email];
    connection.query(query, queryData, function (err, result) {
        var account_id = result.insertId;
        var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
        var accountSkillData = [];
        if (params.skill_id.constructor === Array) {
            for (var i = 0; i < params.skill_id.length; i++) {
                accountSkillData.push([account_id, params.skill_id[i]]);
            }
        }
        else {
            accountSkillData.push([account_id, params.skill_id]);
        }
        connection.query(query, [accountSkillData], function(err, result){
            var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';
            var accountSchoolData = [];
            if (params.school_id.constructor === Array) {
                for (var i = 0; i < params.school_id.length; i++) {
                    accountSchoolData.push([account_id, params.school_id[i]]);
                }
            }
            else {
                accountSchoolData.push([account_id, params.school_id]);
            }
            connection.query(query, [accountSchoolData], function(err, result){
                var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';
                var accountCompanyData = [];
                if (params.company_id.constructor === Array) {
                    for (var i = 0; i < params.company_id.length; i++) {
                        accountCompanyData.push([account_id, params.company_id[i]]);
                    }
                }
                else {
                    accountCompanyData.push([account_id, params.company_id]);
                }
                connection.query(query, [accountCompanyData], function(err, result){
                    callback(err, result);
                });
            });
        });
    });
};