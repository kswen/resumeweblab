var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(address_id, callback) {
    var query = 'SELECT street, zip_code FROM address ' +
        'WHERE address.address_id = ?';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO address (street, zip_code) VALUES ?';
    var queryData = [params.street, params.zip_code];

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var addressData = [];
    if (params.zip_code.constructor === Array) {
        for (var i = 0; i < params.zip_code.length; i++) {
            addressData.push([params.street, params.zip_code[i]]);
        }
    }
    else {
        addressData.push([params.street, params.zip_code]);
    }

    // NOTE THE EXTRA [] AROUND companyAddressData
    connection.query(query, [addressData], function (err, result) {
        callback(err, result);
    });
};


exports.delete = function(address_id, callback) {
    var query = 'DELETE FROM address WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE address SET street = ?, zip_code = ? WHERE address_id = ?';
    var queryData = [params.street, params.zip_code, params.address_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.edit = function(address_id, callback) {
    var query = 'CALL address_getinfo(?)';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};